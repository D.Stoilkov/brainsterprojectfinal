<?php

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\StudyFields;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\TutorialController;
use App\Http\Controllers\TutotialController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// StudyFields
Route::get('/', [StudyFields::class, 'programmingStudyFields'])->name('programming');
Route::get('datascience', [StudyFields::class, 'dataScienceStudyFields'])->name('datascience');
Route::get('devops', [StudyFields::class, 'devOpsStudyFields'])->name('devops');
Route::get('design', [StudyFields::class, 'designStudyFields'])->name('design');
Route::get('search', [StudyFields::class, 'search'])->name('search');
// Route::get('live_search', [StudyFields::class, 'search'])->name('live_search');
// Route::get('live_search/action', [StudyFields::class, 'action'])->name('live_search.action');


// Socalite Login
Route::get('login/github', [LoginController::class, 'redirectToGithub'])->name('login.github');
Route::get('login/github/callback', [LoginController::class, 'handleGithubCallback']);

Route::get('login/facebook', [LoginController::class, 'redirectToFacebook'])->name('login.facebook');
Route::get('login/facebook/callback', [LoginController::class, 'handleFacebookCallback']);

Route::get('login/google', [LoginController::class, 'redirectToGoogle'])->name('login.google');
Route::get('login/google/callback', [LoginController::class, 'handleGoogleCallback']);

Route::put('tutorial', [TutorialController::class, 'storeTutorial'])->name('tutorial');

Route::get('tutorial/{name}', [TutorialController::class, 'show']);

Route::get('admin', [AdminController::class, 'index'])->name('admin')->middleware('Admin');

// Route::get('admin/{id}', [AdminController::class, 'update'])->middleware('Admin');








