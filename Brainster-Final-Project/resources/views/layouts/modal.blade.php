<!-- tutorialModal -->
<div class="modal tutorial-modal" tabindex="-1" role="dialog" id="tutorialModal">
    <div class="modal-dialog" role="document">
        <i class="fas fa-times-circle close fa-2x" data-dismiss="modal" aria-label="Close"></i>
        <div class="modal-content">
            @if(!Auth::check())
            <div class="modal-header text-center flex-column position-relative border-bottom-0">
                <h2 class="modal-title font-weight-bold w-100">Welcome to Brainster</h2>
                <p class="modal-subtitle text-muted w-100">Signup to submit and upvote tutorials, follow topics, and more.</p>
                <span class="modal-subtitle text-muted w-100 m-3">You need to Sign up to submit a Tutorial</span>
                <span class="modal-subtitle text-muted w-100 border-bottom">CONTINUE WITH:</span>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form action="{{ route('register') }}" method="POST">
                        @csrf

                        <div class="form-group row d-flex justify-content-center">
                            <a class="btn btn-primary pl-0 pt-0 pb-0 shadow" href="{{ route('login.google')}}" role="button"><img src="/img/image-20150902-6700-t2axrz.jpg" style="width: 45px; height: 45px;">Sign in with Google</a>
                        </div>

                        <div class="form-group row p-0">
                            <div class="col-md-6 pl-0">
                                <a class="btn btn-primary btn-block shadow" href="{{ route('login.facebook')}}" role="button"><i class="fab fa-facebook-square  mr-2"></i>Facebook</a>
                            </div>

                            <div class="col-md-6 pr-0">
                                <a class="btn btn-dark btn-block shadow" href="{{ route('login.github')}}" role="button"><i class="fab fa-github mr-2"></i>GitHub</a>
                            </div>
                        </div>

                        <div class="devider">
                            <span class="devider-text">or</span>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <span>
                                    <i class="fas fa-user text-muted" id="userIcon"></i>
                                </span>

                                <input placeholder="Full Name" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <i class="fas fa-envelope text-muted" id="envelopeIcon"></i>
                                <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <i class="fas fa-lock text-muted" id="lockIcon"></i>

                                <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <div class="g-recaptcha" data-sitekey="6LflFaIaAAAAAPS_pr2T64TP346MESiiBvq3EiSj"></div>
                            </div>
                        </div>

                        <div class="form-group row mb-0 mt-3">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Create Account') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer border-top-0">
                <div class="col">
                    <p class="text-center">Already have an account?<a id="LoginLink" class="ml-1">Login</a></p>
                </div>
            </div>
            @else
            <div class="modal-header text-center flex-column position-relative border-bottom-0">
                <h2 class="modal-title font-weight-bold w-100">Submit a Tutorial/Course</h2>
                <p class="modal-subtitle text-muted w-100">Feel free to submit tutorial in any language</p>
                <p class="modal-subtitle text-muted continue-text  w-100 border-bottom">Paste the link below:</p>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form action="{{ route('tutorial') }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <div class="col-md-12 ">
                                <i class="fas fa-link" id="linkIcon"></i>
                                <input placeholder="URL of the tutorial" id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{ old('url') }}" required autocomplete="url">
                                @error('url')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <p class="tutorialtext">Tell us more about this 'tutorial/course' (optional)</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 mt-2 mb-3">
                                <label for="submitTutorialInput">Category</label>
                                <input type="text" class="form-control" placeholder="Python, Angular, etc." id="submitTutorialInput" name="category">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-2"><span>Tags:</span></div>

                            <div class="col offset-3">
                                <div class="form-check form-check-inline">

                                    <input class="form-check-input" type="checkbox" id="free" value="free" name="tags[]">
                                    <label class="form-check-label" for="CheckboxFree">Free</label>
                                       
                                    <input class="form-check-input ml-2" type="checkbox" id="paid" value="paid" name="tags[]">
                                    <label class="form-check-label" for="CheckboxPaid">Paid</label>

                                    <input class="form-check-input ml-2" type="checkbox" id="video" value="video" name="tags[]">
                                    <label class="form-check-label" for="CheckboxVideo">Video</label>

                                    <input class="form-check-input ml-2" type="checkbox" id="book" value="book" name="tags[]">
                                    <label class="form-check-label" for="CheckboxBook">Book</label>
                                </div>
                                
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col"><span>This tutorial is for:</span></div>

                            <div class="col">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="level[]" id="advanced" value="advanced">
                                    <label class="form-check-label" for="RadioAdvanced">Advanced</label>
                                     
                                    <input class="form-check-input ml-2" type="radio" name="level[]" id="beginner" value="beginner">
                                    <label class="form-check-label" for="RadioBeginner">Beginner</label>
                                </div>
                              
                            </div>
                        </div>

                        <div class="form-group row mb-0 mt-3">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="row mt-3">
                        <div class="col">
                            <p class="text-center">Have you read our submission<a href="#" id="linkGuide" class="pl-1">guidelines</a>?</p>
                        </div>
                    </div>

                </div>
            </div>
            @endif

        </div>
    </div>
</div>
<!-- End tutorialModal -->


<!-- Sign In Modal -->
<div class="modal sign-modal" tabindex="-1" role="dialog" id="signInModal">
    <div class="modal-dialog" role="document">
        <i class="fas fa-times-circle close fa-2x" data-dismiss="modal" aria-label="Close"></i>
        <div class="modal-content">
            <div class="modal-header text-center flex-column position-relative border-bottom-0">
                <h2 class="modal-title font-weight-bold w-100">Welcome Back</h2>
                <span class="modal-subtitle text-muted w-100 border-bottom">CONTINUE WITH:</span>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row d-flex justify-content-center">
                            <a class="btn btn-primary pl-0 pt-0 pb-0 shadow" href="{{ route('login.google')}}" role="button"><img src="/img/image-20150902-6700-t2axrz.jpg" style="width: 45px; height: 45px;">Sign in with Google</a>
                        </div>

                        <div class="form-group row p-0">
                            <div class="col-md-6 pl-0">
                                <a class="btn btn-primary btn-block shadow" href="{{ route('login.facebook')}}" role="button"><i class="fab fa-facebook-square  mr-2"></i>Facebook</a>
                            </div>

                            <div class="col-md-6 pr-0">
                                <a class="btn btn-dark btn-block shadow" href="{{ route('login.github')}}" role="button"><i class="fab fa-github mr-2"></i>GitHub</a>
                            </div>
                        </div>

                        <div class="devider">
                            <span class="devider-text">or</span>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <i class="fas fa-envelope text-muted" id="envelopeIcon"></i>
                                <input placeholder="Email" id="loginemail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <i class="fas fa-lock text-muted" id="lockIcon"></i>
                                <input placeholder="Password" id="loginpassword" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <div class="g-recaptcha" data-sitekey="6LflFaIaAAAAAPS_pr2T64TP346MESiiBvq3EiSj"></div>
                            </div>
                        </div>

                        @if (Route::has('password.request'))
                        <a class="btn btn-link d-flex justify-content-end" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                        @endif

                        <div class="form-group row mb-0 mt-3">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>

                    </form>

                     <div class="row mt-3">
                        <div class="col">
                            <p class="text-center">Don't have an account?<a href="#" id="SignUpLink">Sign Up</a></p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


<!-- End Sign In Modal -->





















