<!-- NavBar -->
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="/img/Brainster- symbol 32x32-02.png" alt="">
            <!-- <img src="/img/Brainster- symbol 310x150-02.png" alt="" style="width: 100px;"> -->
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

                <li class="nav-item d-inline-flex ">
                    <a class="nav-link {{request()->is('/') ? 'active' : ''}}" href="{{ route('programming')}}">Programming<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item d-inline-flex ">
                    <a class="nav-link {{request()->is('datascience') ? 'active' : ''}}" href="{{ route('datascience')}}">Data Science</a>
                </li>
                <li class="nav-item d-inline-flex">
                    <a class="nav-link {{request()->is('devops') ? 'active' : ''}}" href="{{ route('devops')}}">DevOps</a>
                </li>
                <li class="nav-item d-inline-flex ">
                    <a class="nav-link {{request()->is('design') ? 'active' : ''}}" href="{{ route('design')}}">Design</a>
                </li>

                @if (Auth::check() && Auth::user()->role == 'admin')
                <li class="nav-item d-inline-flex ">
                    <a class="nav-link {{request()->is('design') ? 'active' : ''}}" href="{{ route('admin')}}">Tutorials</a>
                </li>

                @endif

            </ul>

            <div class="form-group input-group-sm col-md-5 mb-0">
                <form action="{{ route('search') }}" method="GET">
                    <input type="search" class="form-control" placeholder="Search for topics" id="searchMain" name="searchMain">

                </form>

            </div>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <button class="btn btn-sm" type="button" data-toggle="modal" data-target="tutorialModal" id="SubmitTutorialButton"><i class="fas fa-plus mr-1"></i>{{ __('Submit a tutorial') }}</button>
                </li>


                @if(Auth::check())
                <div class="">
                    <div class="col d-inline-flex">
                        <i class="fas fa-bookmark p-1"></i>
                        <i class="fas fa-bell p-1"></i>
                    </div>
                </div>

                <li class="nav-item dropdown">

                    <a id="navbarDropdown" class="nav-link dropdown-toggle btn-sm p-1" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
                @else
                <li class="nav-item">
                    <button class="btn btn-sm" type="button" data-toggle="modal" data-target="SignUpModal" id="SignButton">{{ __('Sign Up/Sign In') }}</button>
                </li>
                @endif

            </ul>
        </div>
    </div>
</nav>
<!-- End NavBar -->