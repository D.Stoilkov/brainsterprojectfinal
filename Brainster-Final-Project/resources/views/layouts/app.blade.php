<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')

<body>
    <div id="app">
        @include('layouts.navbar')



        <main class="py-4">
            @yield('content')
        </main>

        @include('layouts.modal')
    </div>
</body>

</html>


<script>
    $(document).ready(function() {
        $("#SubmitTutorialButton").click(function() {
            $("#tutorialModal").modal('show')
        })

        $("#SignButton").click(function() {
            $("#tutorialModal").modal('show')
        })

        $("#LoginLink").click(function() {
            $("#tutorialModal").modal('hide')
            $("#signInModal").modal('show')
        })

        $("#SignUpLink").click(function() {
            $("#signInModal").modal('hide')
            $("#tutorialModal").modal('show')
        })

     
    

    })
</script>