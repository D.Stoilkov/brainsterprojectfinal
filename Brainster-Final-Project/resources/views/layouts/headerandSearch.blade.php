<div class="row">
    <div class="col-md-12 text-center">
        <h1 class="mb-3 mt-5 ">Find the Best Programming Courses & Tutorials</h1>
        <form  action="#" method="GET">
            <div class="form-holder ml-3 mr-3" id="searchWrapper">
                <i class="fas fa-search pt-1" id="searchIcon"></i>
                <input type="search" class="form-control" placeholder="Search for the language you want to learn: Python, Javascript" name="search" id="search">
            </div>
        </form>
    </div>
</div>

