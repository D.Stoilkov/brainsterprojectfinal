@extends('layouts.app')

@section('content')
<div class="container">
    @include('layouts.headerandSearch')

    <div class="row mt-5 ">
        @foreach($results as $searchresult)

        <div class="col-md-4 mb-2 animate-card">
            <a href="tutorial/{{$searchresult->name}}" class="text-dark">
                <div class="card {{$searchresult->name}} rounded shadow-sm">
                    <div class="card-body p-0 d-flex flex-row align-items-center">
                        <div class="col-md-4 p-3">
                            <img src="{{$searchresult->images_path}}" alt="" class="img-fluid" style="height: 50px;">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$searchresult->name}}</h5>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        @endforeach
    </div>
</div>

@endsection


