@extends('layouts.app')


@section('content')

<div class="container tutorial-header">
   <div class="row p-5">
      <div class="col-md-2">
         <img src="{{$result->images_path}}" alt="" class="img-fluid" style="height: 100px;">
      </div>
      <div class="col-md-10 pt-3">
         <h2>{{$result->name}} Tutorials and courses</h2>
         <p>Learn online from the best tutorials submited & voted by the comunity</p>
      </div>
   </div>

</div>
<div class="container mt-5">

   <div class="row">
      <div class="col-md-3">
         <form action="{{URL::current()}}" method="GET">
            <div class="card border border-primary">
               <div class="card-header d-flex justify-content-between">
                  Filter Courses
                  <button type="submit" class="btn btn-primary btn-sm" id="filterTutorial">Filter</button>
               </div>
               <div class="card-body">
                  <div class="category">
                     <input type="checkbox" name="filter" id="freeinp" value="free">
                     <label for="free" class="">Free</label>
                  </div>
                  <div class="category">
                     <input type="checkbox" name="filter" id="paidinp" value="paid">
                     <label for="paid" class="">Paid</label>
                  </div>

                  <div class="category">
                     <input type="checkbox" name="filter" id="videoinp" value="video">
                     <label for="video" class="">Video</label>
                  </div>

                  <div class="category">
                     <input type="checkbox" name="filter" id="bookinp" value="book">
                     <label for="book" class="">Book</label>
                  </div>

                  <div class="category">
                     <input type="checkbox" name="filter" id="advanced" value="advanced">
                     <label for="advanced" class="">Advanced</label>
                  </div>

                   <div class="category">
                     <input type="checkbox" name="filter" id="beginner" value="beginner">
                     <label for="beginner" class="">Beginner</label>
                  </div>

               </div>
            
            </div>



         </form>

      </div>
      <div class="col-md-9">
         @foreach($filterTutorials as $tutorial)
         <div class="card mb-3">
            <div class="row no-gutters">
               <div class="col-md-3 ">
                  <img src="/img/Brainster- symbol 310x150-02.png" alt="" class="img-fluid">
                  @if(Auth::check())
                  <div class="text-center d-flex justify-content-around">

                     <form action="" method="POST">

                        <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-thumbs-up"><span class="ml-2 font-weight-normal">Like</span></i></button>
                     </form>

                     <form action="" method="POST">

                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-thumbs-down "><span class="ml-2 font-weight-normal">Dislike</span></i></button>

                     </form>

                  </div>
                  @endif
               </div>

               <div class="col-md-9">
                  <div class="card-body ">
                     <h4 class="card-title d-flex justify-content-between">{{$result->name}} Tutorial {{$tutorial->id}}<a href="{{$tutorial->url}}" class="btn-sm btn-primary">View</a></h4>
                     <div class="card-text">
                        <button type="button" class="btn btn-sm btn-success">{{$tutorial->tags}}</button>
                        <button type="button" class="btn btn-sm btn-success">{{$tutorial->level}}</button>
                     </div>
                     <p class="card-text mt-2 mb-1"><small class="text-muted">created at: {{$tutorial->created_at}}</small></p>
                  </div>
               </div>
            </div>
         </div>
         @endforeach
         {!! $filterTutorials->appends(request()->query())->links() !!}
        
      </div>
   </div>
</div>


@endsection

