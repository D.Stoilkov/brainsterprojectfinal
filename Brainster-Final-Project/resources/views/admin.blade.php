<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/9f78ce844a.js" crossorigin="anonymous"></script>

    <!-- jQurey -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <!-- reCAPTCHA  -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
    <!-- NavBar -->
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/img/Brainster- symbol 32x32-02.png" alt="">

            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                    <li class="nav-item d-inline-flex ">
                        <a class="nav-link {{request()->is('/') ? 'active' : ''}}" href="{{ route('programming')}}">Programming<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item d-inline-flex ">
                        <a class="nav-link {{request()->is('datascience') ? 'active' : ''}}" href="{{ route('datascience')}}">Data Science</a>
                    </li>
                    <li class="nav-item d-inline-flex">
                        <a class="nav-link {{request()->is('devops') ? 'active' : ''}}" href="{{ route('devops')}}">DevOps</a>
                    </li>
                    <li class="nav-item d-inline-flex ">
                        <a class="nav-link {{request()->is('design') ? 'active' : ''}}" href="{{ route('design')}}">Design</a>
                    </li>


                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">

                    @if(Auth::check())
                    <div class="">
                        <div class="col d-inline-flex">
                            <i class="fas fa-bookmark p-1"></i>
                            <i class="fas fa-bell p-1"></i>
                        </div>
                    </div>

                    <li class="nav-item dropdown">

                        <a id="navbarDropdown" class="nav-link dropdown-toggle btn-sm p-1" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @else
                    <li class="nav-item">
                        <button class="btn btn-sm" type="button" data-toggle="modal" data-target="SignUpModal" id="SignButton">{{ __('Sign Up/Sign In') }}</button>
                    </li>
                    @endif

                </ul>
            </div>
        </div>
    </nav>
    <!-- End NavBar -->


    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tutorial URL</th>
                            <th scope="col">Category</th>
                        </tr>
                    </thead>
                    <tbody> 
                        @foreach($tutorial as $info)
                        <tr>
                            <td>{{$info->id}}</td>
                            <td><a href="{{$info->url}}">{{$info->url}}</a></td>
                            <td>{{$info->category}}</td>
            
                            <td><a href="#" class="btn btn-sm btn-success">Approve</a></td>
                            <td><a href="#" class="btn btn-sm  btn-danger">Reject</a></td>
                        </tr>
                        @endforeach
                      
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>