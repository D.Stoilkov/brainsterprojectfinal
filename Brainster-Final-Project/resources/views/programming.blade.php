@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <div class="container">
        @include('layouts.headerandSearch')

        <div class="row mt-5 ">
            @foreach($topics as $topic)

            <div class="col-md-4 mb-2 animate-card">
                <a href="tutorial/{{$topic->name}}" class="text-dark">
                    <div class="card {{$topic->name}} rounded shadow-sm">
                        <div class="card-body p-0 d-flex flex-row align-items-center">
                            <div class="col-md-4 p-3">
                                <img src="{{$topic->images_path}}" alt="" class="img-fluid" style="height: 50px;">
                            </div>
                            <div class="col-md-8">
                                <h5>{{$topic->name}}</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            @endforeach
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#search').on('keyup', function() {
            $value = $(this).val();
            $.ajax({
                type: 'GET',
                url: 'http://127.0.0.1:8000',
                data: {
                    'search': $value
                },
                success: function(data) {
                    console.log(data)
                }
            });


        })

     

    })
</script>



@endsection


