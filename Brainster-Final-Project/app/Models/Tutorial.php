<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class Tutorial extends Model
{
    // use HasFactory;

    protected $fillable = [
        'url',
        'category',
        'tags',
        'level',
    ];

    // protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // public function like($user = null, $liked = true)
    // {
    //     $this->likes()->updateOrCreate([
    //         'user_id' => $user ? $user->id : auth()->id(),
    //     ], [
    //         'liked' => $liked,
    //     ]);
    // }

    // public function dislike($user = null)
    // {
    //     return $this->like($user, false);
    // }

    // public function isLikeBy(User $user)
    // {
    //     return (bool)
    //     $user->likes->where('tutorials_id', $this->id)
    //         ->where('liked', true)
    //         ->count();
    // }


    // public function isDisLikeBy(User $user)
    // {
    //     return (bool)
    //     $user->likes->where('tutorials_id', $this->id)
    //         ->where('liked', false)
    //         ->count();
    // }


    // public function likes()
    // {
    //     return $this->hasMany(Like::class);
    // }

    // public function scopeWithLikes(Builder $query)
    // {
    //     // $query->leftJoinSub(
    //     //     '(select tutorials_id, sum(liked) likes, sum(!liked) dislikes from likes group by tutorial_id)';
    //     // )
    // }
}
