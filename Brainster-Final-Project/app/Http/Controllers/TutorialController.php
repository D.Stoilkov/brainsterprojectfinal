<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Models\Tutorial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;
use SocialiteProviders\Manager\OAuth1\User;

class TutorialController extends Controller
{

   public function storeTutorial(Request $request)
   {

      //   dd($request->all());
      $tutorial = new Tutorial();
      $tutorial->url = $request->url;
      $tutorial->category = $request->category;
      $tutorial->tags = implode(',', $request->input('tags'));
      $tutorial->level = implode(',', $request->level);
      $tutorial->save();

      return redirect()->route('programming');
   }

   public function show($name, Request $request)
   {
      $result = Topic::where('name', $name)->first();

      $filter = $request->filter;
      // dd($filter);

      if (in_array('beginner', array($filter))) {
         $filterTutorials = Tutorial::where('category', 'LIKE', "%$name%")->where('level', 'LIKE', "%$request->filter%")->simplePaginate(4);
      } elseif (in_array('advanced', array($filter))) {
         $filterTutorials = Tutorial::where('category', 'LIKE', "%$name%")->where('level', 'LIKE', "%$request->filter%")->simplePaginate(4);
      } else {
         $filterTutorials = Tutorial::where('category', 'LIKE', "%$name%")->where('tags', 'LIKE', "%$request->filter%")->simplePaginate(4);
      }

      // $filterTutorials = Tutorial::paginate(4);


      //  dd($tutorials);
      //   $tutorials = Tutorial::paginate(3);
      // $tutorials = Tutorial::where('category', 'LIKE', "%$name%")->get();

      // $likedTutorial = DB::select( DB::raw('SELECT * FROM `tutorials` LEFT JOIN ( SELECT tutorials_id, sum(liked) likes, sum(!liked) dislikes FROM likes GROUP BY tutorials_id ) likes ON likes.tutorials_id = tutorials_id'));
      // //   dd( $likedTutorial);
      //  $likedTutorial = json_encode($likedTutorial, true);
      //  dd($likedTutorial);
      return view('courses', compact('result', 'filterTutorials', 'filter'));
   }
}
