<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use phpDocumentor\Reflection\Types\Null_;

class StudyFields extends Controller
{
    public function programmingStudyFields(Request $request)
    {
        if ($request->ajax()) {
            return $topics = Topic::where('name', 'LIKE', "%$request->search%")->get();
            $topics = json_decode($topics, true);
        }

        $topics = Topic::where('name', 'LIKE', "%$request->search%")->where('category', '=', 'Programming')->get();

        //  $all = Topic::all('name');  

        return View::make('programming')->with('topics', $topics);
    }

    public function dataScienceStudyFields(Request $request)
    {
        if ($request->ajax()) {
            return $scienceData = Topic::where('name', 'LIKE', "%$request->search%")->get();
            $scienceData = json_decode($scienceData, true);
        }

        $scienceData = Topic::where('name', 'LIKE', "%$request->search%")->where('category', '=', 'Data Science')->get();

        return view('datascience', compact('scienceData'));
    }

    public function devOpsStudyFields(Request $request)
    {


        if ($request->ajax()) {
            return $devops = Topic::where('name', 'LIKE', "%$request->search%")->get();
            $devops = json_decode($devops, true);
        }

        $devops = Topic::where('name', 'LIKE', "%$request->search%")->where('category', '=', 'Dev Ops')->get();


        return view('devops', compact('devops'));
    }

    public function designStudyFields(Request $request)
    {
        if ($request->ajax()) {
            return $desin = Topic::where('name', 'LIKE', "%$request->search%")->get();
            $desin = json_decode($desin, true);
        }

        $desin = Topic::where('name', 'LIKE', "%$request->search%")->where('category', '=', 'Design')->get();

        // $desin = Topic::where('category', '=', 'Design')->get();

        return view('design', compact('desin'));
    }


    public function search(Request $request)
    {

        $results = Topic::where('name', 'LIKE', "%$request->searchMain%")
            ->orWhere('category', 'LIKE', "%$request->searchMain%")
            ->get();

        return view('search', compact('results'));
    }
}
